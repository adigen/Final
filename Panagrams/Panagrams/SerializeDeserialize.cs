﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Panagrams
{
    public class SerializeDeserialize
    {
        private static Dictionary<string, string> toSerialize;

        public static void SavePanagram(string inputString, string response)
        {   
            SaveOrCreateDictionary(inputString, response);
            SaveDictionary();
        }

        private static void SaveDictionary()
        {
            var pathToFile = @"C:\Users\No\Desktop\panagrams.txt";

            using (StreamWriter file = File.CreateText(pathToFile))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, toSerialize);
            }
        }

        private static void SaveOrCreateDictionary(string panagram, string response)
        {
            try
            {
                Dictionary<string, string> toDeserialize = new Dictionary<string, string>();
                toSerialize = LoadPanagram(toDeserialize);
                toSerialize.Add(panagram, response);
            }
            catch (System.IO.IOException e)
            {
                Debug.WriteLine("Error: {0}", e.Message);
            }
            finally
            {
                if (toSerialize == null)
                {
                    toSerialize = new Dictionary<string, string>();
                    toSerialize.Add(panagram, response);
                }
            }
        }

        public static Dictionary<string, string> LoadPanagram(Dictionary<string, string> toDeserialize)
        {
            var pathToFile = @"C:\Users\No\Desktop\panagrams.txt";
            try
            {
                using (StreamReader file = File.OpenText(pathToFile))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    toDeserialize = JsonConvert.DeserializeObject<Dictionary<string, string>>(file.ReadToEnd());
                }
            }
            catch (FileNotFoundException e)
            {
                Debug.WriteLine("Error: {0}", e.Message);
            }

            return toDeserialize;
        }

        public static void RemoveById(int id)
        {
            Dictionary<string, string> toDeserialize = new Dictionary<string, string>();
            toSerialize = SerializeDeserialize.LoadPanagram(toDeserialize);

            toSerialize.Remove(toSerialize.ElementAt(id).Key);

            SaveDictionary();
        }
    }
}
