﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Panagrams.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            Dictionary<string, string> toDeserialize = new Dictionary<string, string>();
            var panagramsDeserialized = SerializeDeserialize.LoadPanagram(toDeserialize);

            foreach (KeyValuePair<string, string> item in panagramsDeserialized)
            {
                yield return item.Key + ": " + item.Value;
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            Dictionary<string, string> toDeserialize = new Dictionary<string, string>();
            var panagramsDeserialized = SerializeDeserialize.LoadPanagram(toDeserialize);

            return panagramsDeserialized.ElementAt(id).Key;
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody]Panagrams jsonPanagram)
        {
            SerializeDeserialize.SavePanagram(jsonPanagram.inputString, jsonPanagram.SeeIfItsAPanagrams());

            return jsonPanagram.SeeIfItsAPanagrams();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Panagrams jsonPanagram)
        {
            SerializeDeserialize.RemoveById(id);
            SerializeDeserialize.SavePanagram(jsonPanagram.inputString, jsonPanagram.SeeIfItsAPanagrams());
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            SerializeDeserialize.RemoveById(id);
        }
    }
}
