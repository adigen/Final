﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Panagrams
{
    public class Panagrams
    {
        public string inputString;

        public Panagrams(string inputString)
        {
            this.inputString = inputString;
        }

        public Panagrams()
        {

        }

        public string SeeIfItsAPanagrams()
        {
            var response = GetUniqueCharacters(EliminateNonAlpha(inputString.ToLower())).Length == 26 ? "Yes, this is a panagram" : "Not a panagram";

            return response;
        }

        private string EliminateNonAlpha(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < inputString.Length; i++)
            {
                if (char.IsLetterOrDigit(inputString[i]))
                {
                    sb.Append(inputString[i]);
                }
            }
            return sb.ToString();
        }

        private string GetUniqueCharacters(string inputString)
        {
            var finalString = String.Join("", inputString.Distinct());

            return finalString;
        }
    }
}