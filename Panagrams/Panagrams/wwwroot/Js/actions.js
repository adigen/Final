﻿function verifyButtonAction()
{
    $('#verifyBtn').click(function () {
        moveData(values, "text");
        postRequest(values);
    });
}

﻿function buttonTogle() {
    $("#results").hide();

    $(document).ready(function () {
        $("#display").click(function () {
            $("#results").toggle();
        });
    });
 }

function displayTable(data) {
    $('#results').empty();
    $('#results').append('There are following attemps: ' + '<br>');
    Object.keys(data).forEach(function (key) {

        createDeleteButton(key);
        createEditButton(key);
        $("#results")
            .append('<br>' + data[key] + '<br>');      
    });
 }

function deletePanagramBtnOnClick(id, idRequest) {
    $('#' + id).click(function () {
        deleteRequest(idRequest);
    });
}

function postResponse(data) {
    if (data === "Not a panagram") {
        $("#resultPanagrams").text(values.inputString + " is not a panagram.");
    }
    else {
        $("#resultPanagrams").text(values.inputString + " is a panagram.");
    }
}

function editPanagramBtnOnClcik(id, idRequest) {
    $('#' + id).click(function () {
        getById(idRequest);
        moveId(idRequest);
    });
}

function mainPageToggleEditPage() {
    $("#edit").hide();

    $(document).on("click", 'input[value=\'Try Again\'], #editBtn', function () {
        $("#main, #results, #edit").toggle();
    });
}

function panagramTryYourLuckBtnOnClick() {
    $("#editBtn").click(function () {
        moveData(values, "textEdit");
        if (values.inputString !== "")
            putRequest(values);
    });
}