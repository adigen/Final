﻿//post
function postRequest(values) {
    $.ajax({
        type: "POST",
        url: 'api/values',
        contentType: 'application/json',
        data: JSON.stringify(values),
        success: function (data) {
            postResponse(data);
            getRequest();
        }
    });
}

//get
function getRequest() {
    $.ajax({
        type: 'GET',
        url: 'api/values',
        success: function (data) {
            displayTable(data);
        }
    });
}

const interval = setInterval(function () { getRequest(); }, 60000);

﻿//detelete
function deleteRequest(id) {
    $.ajax({
        type: 'DELETE',
        url: `api/values/${id}`,
        success: function () {
            getRequest();
        }
    });
}

//getbyid
function getById(id) {
    $.ajax({
        type: 'GET',
        url: `api/values/${id}`,
        success: function (data) {
            $("#textEdit").val(data);
        }
    });
}

//put
function putRequest(values) {
    $.ajax({
        type: "PUT",
        url: `api/values/${values.id}`,
        contentType: 'application/json',
        data: JSON.stringify(values),
        success: function () {
            getRequest();
        }
    });
}