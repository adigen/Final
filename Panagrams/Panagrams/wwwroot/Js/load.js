﻿function loadElements() {
    createElementjQuery('body', '<center><h3>Panagrams Verifier</h3></center>');
    createElementjQuery('body', '<div id=\'main\'></div>');
    createElementjQuery('#main', '<input id=\'text\' placeholder=\'Try a panagram\'/>');
    createButonjQuery("#main", "verifyBtn", "Verify");
    createElementjQuery('#main', '<p id=\'resultPanagrams\'></p>');
    createElementjQuery('body', '<div id=\'results\'></div>');
    createButonjQuery("#main", "display", "Show history");
    createElementjQuery('body', '<div id=\'edit\'></div>');
    createElementjQuery('#edit', '<input id=\'textEdit\' placeholder=\'Try again\'/>');
    createButonjQuery('#edit', 'editBtn', 'Try your luck');
}

function loadClickActions() {
    verifyButtonAction();
    buttonTogle();
    mainPageToggleEditPage();
    panagramTryYourLuckBtnOnClick();
}

function requests(){
    getRequest();
}