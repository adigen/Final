﻿﻿function createElementjQuery(location, element) {
    $(location).append(element);
}


function createButonjQuery(location, btnId, btnValue) {
     let $but = $('<input/>').attr({ type: 'button', id: btnId, value: btnValue });

     $(location).append($but);
}

function createDeleteButton(idRequest) {
    let id = 'del' + idRequest;
    createButonjQuery('#results', id, 'Delete Alarm');
    deletePanagramBtnOnClick(id, idRequest);
}

function createEditButton(idRequest) {
    let id = 'edt' + idRequest;
    createButonjQuery('#results', id, 'Try Again');
    editPanagramBtnOnClcik(id, idRequest);
}