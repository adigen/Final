using Xunit;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Panagrams
{
    public class PanagramsTests
    {
        [Fact]
        public void TestForPanagram()
        {
            var panagram = new Panagrams("abcdefghijklmnopqrstuvwxyz");

            Xunit.Assert.Equal("Yes, this is a panagram", panagram.SeeIfItsAPanagrams());
        }

        [Fact]
        public void TestForUniqueCharacters()
        {
            PrivateObject obj = new PrivateObject(typeof(Panagrams));
            var result = obj.Invoke("GetUniqueCharacters", "The quick brown fox jumps over the lazy dog");

            Xunit.Assert.Equal("The quickbrownfxjmpsvtlazydg", result);
        }

        [Fact]
        public void TestForASentence()
        {
            var panagram = new Panagrams("The quick brown fox jumps over the lazy dog");
            Xunit.Assert.Equal("Yes, this is a panagram", panagram.SeeIfItsAPanagrams());
        }

        [Fact]
        public void TestForNotAPanagram()
        {
            var panagram = new Panagrams("the quick brown fox");

            Xunit.Assert.Equal("Not a panagram", panagram.SeeIfItsAPanagrams());
        }

        [Fact]
        public void TestForEliminateNonAlpha()
        {
            PrivateObject obj = new PrivateObject(typeof(Panagrams));
            var result = obj.Invoke("EliminateNonAlpha", "a + b + c");

            Xunit.Assert.Equal("abc", result);
        }

        [Fact]
        public void TestForNoPanagramSpecialCharacters()
        {
            var panagram = new Panagrams("abcdefghijklmnopqrstuvwxy ");

            Xunit.Assert.Equal("Not a panagram", panagram.SeeIfItsAPanagrams());
        }
    }
}
